-- load and write files
vim.opt.autowrite = true
vim.opt.autoread = true

-- use spaces for tabs and whatnot
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true

-- vim.opt.wrap = false

-- file diff tracking
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- search
--vim.opt.hlsearch = false
vim.opt.incsearch = true

-- workarea
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

-- show whitespaces
vim.opt.list = true
vim.opt.listchars = 'tab:>-,trail:-,extends:#,nbsp:-'

-- show line numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- code guiding
vim.opt.tw = 78
