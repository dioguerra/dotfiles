vim.g.mapleader = " "

vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>', { noremap = true, silent = true })

-- move block of text together
--vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
--vim.keymap.set("v", "k", ":m '<-2<cr>gv=gv")
--
--vim.keypap.set("n",,)
-- keep cursor on left when concating
vim.keymap.set("n", "J", "mzJ`z")
-- center cursor when jumpting page
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
-- center search terms
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
-- system copy
vim.keymap.set({"n", "v"}, "<leader>y", "\"*y")
--need to fix provider clipboard
-- delete without saving to register
--vim.keymap.set({"n", "v"}, "<leader>d", "\"_d")
