-- https://github.com/nvim-treesitter/nvim-treesitter

require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  ensure_installed = { "vim", "lua", "c", "rust" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,
  auto_install = true,
  ignore_install = {},
  highlight = {
    enable = true,
  },
  indent = {
    enable = true,
  },
  additional_vim_regex_highlighting = false,
}

-- Install packet
-- TSInstall <package_name>
