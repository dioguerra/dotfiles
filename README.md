# Dotfiles
My dotfiles managed by chezmoi

# Dependency
Install git
```bash
apt install -y git
```

# Bootstrap
Default configurations will be minimal and will not include development tools.
If you wish to configure before full installation:
1. `sh -c "$(curl -fsLS get.chezmoi.io)" -- init https://gitlab.com/dioguerra/dotfiles.git`
2. Edit `~/.config/chezmoi/chezmoi.yaml
3. `chezmoi apply`

Else, to proceed with minimal installation:
```bash
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://gitlab.com/dioguerra/dotfiles.git
```
