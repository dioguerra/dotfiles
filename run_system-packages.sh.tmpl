#!/bin/bash

# development tools
curl -L https://github.com/neovim/neovim-releases/releases/download/stable/nvim-linux64.deb -o nvim-linux.deb &&
sudo apt-get install -y ./nvim-linux.deb &&
rm nvim-linux.deb &&
sudo ln -s /usr/bin/nvim /usr/bin/vim -f &&
sudo apt install -y ripgrep

# container tools
## runner
{{- if .dev.container.runner }}
if command -v docker &> /dev/null; then
    echo "Docker is installed."
elif command -v podman &> /dev/null; then
    echo "Podman is installed."
elif command -v nerdctl &> /dev/null; then
    echo "nerdctl is installed."
else
    echo "No container runner found. Installing Docker."
    sudo apt-get install -y docker.io
    # Your script logic here
fi
{{- end }}

## clusters
### kubectl
{{- if .dev.container.k8s }}
wget "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" -O $HOME/.local/bin/kubectl

### helm
HELM_LATEST=$(curl -s https://github.com/helm/helm/releases | grep "Download Helm" | grep -oP "v[0-9]+\.[0-9]+\.[0-9]+(?=\.)" | head -n1 ) &&
curl https://get.helm.sh/helm-${HELM_LATEST}-linux-amd64.tar.gz -o helm-latest.tar.gz &&
tar zxvf helm-latest.tar.gz &&
mv linux-amd64/helm $HOME/.local/bin
rm -rf helm-latest.tar.gz linux-amd64
{{- end }}

# Security
## LastPass Cli
sudo apt install -y lastpass-cli

# utilities
sudo wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq &&\
sudo chmod +x /usr/bin/yq
